import axios, {AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";
import {BIK_INFO_API_URL, RENT_API_URL} from "./config";

class AxiosClientApi {
    private session: AxiosInstance;

    constructor(apiUrl: string = RENT_API_URL) {
        this.session = axios.create({
            baseURL: apiUrl,
            headers: {
                'Accept-Language': 'ru-RU,ru;',
            },
        });
    }
    public async get<TResponse>(
        url: string, config?:  AxiosRequestConfig
    ): Promise<AxiosResponse<TResponse>> {
        try {
            return await this.session.get(url, config);
        } catch (error) {
            await this.handleError<TResponse>(error);
            throw error;
        }
    }
    public async post<TRequest, TResponse>(
        url: string, params: TRequest, config?: AxiosRequestConfig
    ): Promise<AxiosResponse<TResponse>> {
        try {
            return await this.session.post(url, params, config)
        } catch (error) {
            await this.handleError<TResponse>(error);
            throw error;
        }
    }

    private async handleError<TRequest>(error: unknown) {
        const axiosError = error as AxiosError<TRequest>;

        if (!axiosError) return;
    }
}

export const axiosClient = new AxiosClientApi();
export const bikInfoAxiosClient = new AxiosClientApi(BIK_INFO_API_URL);