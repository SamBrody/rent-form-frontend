import {Control, Controller, FieldValues, Path} from "react-hook-form";
import {Checkbox, FormControlLabel, FormHelperText, Stack, useTheme} from "@mui/material";

type Props <TField extends FieldValues> = {
    name: Path<TField>,
    control: Control<TField>,
    label?: string,
    required?: boolean,
    disabled?: boolean,
};

export const CheckboxField = <
    TField extends FieldValues
> (
    {
        name,
        control,
        label = "Нет договора",
        required = false,
        disabled = false,
    }: Props<TField>
) => {
    const theme = useTheme();

    const labelSx = {fontSize: 13, color: theme.palette.grey["600"]};

    return(
        <Controller
            name={name}
            control={control}
            rules={{required: required && "Необходимо заполнить",}}
            render={({field, fieldState: {error}}) => (
                <Stack>
                    <FormControlLabel
                        control={<Checkbox size="small" disabled={disabled} {...field}/>}
                        sx={labelSx}
                        label={label}
                    />
                    {error && <FormHelperText error>{error.message}</FormHelperText>}
                </Stack>
            )}
        />
    )
}