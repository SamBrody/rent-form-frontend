import {Control, Controller, FieldValues, Path} from "react-hook-form";
import {InputAdornment, TextField, Typography} from "@mui/material";
import {useMask} from "@react-input/mask";
import {ReactNode} from "react";

type Props <TField extends FieldValues> = {
    name: Path<TField>,
    control: Control<TField>,
    regex?: string,
    regexErrorMsg? : string,
    required?: boolean,
    mask?: string,
    replacement?:string,
    placeholder?: string
    width?: number | string,
    endAdornment?: ReactNode,
    onInput?: () => void,
};

export const Input = <
    TField extends FieldValues
> (
    {
        name,
        control,
        regex = "",
        regexErrorMsg = "",
        required = false,
        mask = undefined,
        replacement = undefined,
        placeholder = mask ? mask : "",
        width = 160,
        endAdornment = "",
        onInput = () => {},
    }: Props<TField>
) => {
    const inputRef = useMask({
        mask: mask,
        replacement: replacement
    });

    return (
        <Controller
            name={name}
            control={control}
            rules={{
                required: required && "Необходимо заполнить",
                pattern: {
                    value: new RegExp(regex),
                    message: regexErrorMsg,
                }
            }}
            render={({field, fieldState}) => (
                <TextField
                    onInput={() => onInput()}
                    size="small"
                    sx={{width: width}}
                    inputRef={mask ? inputRef : null}
                    helperText={fieldState.error ? <Typography component="span" fontSize={11}>{fieldState.error.message}</Typography> : null}
                    error={!!fieldState.error}
                    inputProps={{style: {fontSize: 13.5}}}
                    placeholder={placeholder}
                    InputProps={{
                        endAdornment: (endAdornment && <InputAdornment position="end">
                            {endAdornment}
                        </InputAdornment>)
                    }}
                    {...field}
                />
            )}
        />
    );
};