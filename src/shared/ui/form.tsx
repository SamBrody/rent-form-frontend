import {ReactNode} from "react";
import {styled} from "@mui/material";

const StyledForm = styled('form')({
    width: "100%",
});

type Props = {
    children: ReactNode,
    onSubmit: () => void,
}

export const Form = ({children, onSubmit}: Props) => {
    return (
        <StyledForm onSubmit={onSubmit}>
            {children}
        </StyledForm>
    );
};