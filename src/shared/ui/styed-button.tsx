﻿import {Button, styled} from "@mui/material";

export const StyledButton = styled(Button)(() => ({
    siz: "small",
    fontSize: 13,
    textTransform: "none",
}));