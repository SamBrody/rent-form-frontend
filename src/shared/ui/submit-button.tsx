import {ReactNode} from "react";
import {StyledButton} from "./styed-button.tsx";

type Props = {
    children: ReactNode;
}

export const SubmitButton = ({children, ...props} : Props) => {
    return (
        <StyledButton
            type="submit"
            sx={{width: 120}}
            variant="contained"
            color="primary"
            {...props}
        >
            {children}
        </StyledButton>
    );
};