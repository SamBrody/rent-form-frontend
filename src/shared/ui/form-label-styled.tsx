﻿import {FormLabel, styled} from "@mui/material";

export const FormLabelStyled = styled(FormLabel)({
    marginBottom: 5,
    fontSize: 13.5,
});
