import {Control, Controller, FieldValues, Path} from "react-hook-form";
import {DatePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {ruRU} from "@mui/x-date-pickers/locales";
import {Typography} from "@mui/material";

type Props <TField extends FieldValues> = {
    name: Path<TField>,
    control: Control<TField>,
    required?: boolean,
};

export const DatePickerField = <
    TField extends FieldValues
> (
    {
        control,
        name,
        required = false,
    }: Props<TField>
) => {
    return (
        <LocalizationProvider
            localeText={ruRU.components.MuiLocalizationProvider.defaultProps.localeText}
            dateAdapter={AdapterDayjs}
            adapterLocale="ru"
        >
            <Controller
                name={name}
                control={control}
                rules={{required: required && "Необходимо заполнить"}}
                render={({field, fieldState}) => (
                    <DatePicker
                        sx={{width: 160}}
                        slotProps={{
                            clearButton: {size: "small"},
                            openPickerButton: {size: "small"},
                            textField: {
                                InputProps: {style: {fontSize: 13.5}},
                                size: 'small',
                                error: !!fieldState.error,
                                helperText: fieldState.error ? <Typography component="span" fontSize={11}>{fieldState.error.message}</Typography>  : null
                            },
                        }}
                        {...field}
                    />
                )}
            />
        </LocalizationProvider>
    )
}