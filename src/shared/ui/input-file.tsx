import {Control, Controller, FieldValues, Path} from "react-hook-form";
import {FormHelperText, IconButton, InputAdornment, Paper, Stack, TextField, Typography, useTheme} from "@mui/material";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import ClearIcon from "@mui/icons-material/Clear";
import CheckIcon from "@mui/icons-material/Check";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import Dropzone from "react-dropzone";
import {useState} from "react";
import {StyledButton} from "./styed-button.tsx";

const LoadedFile = (
    {
        fileName,
        width,
        isSuccess,
        onCancelClick,
    } : {fileName: string, width: number | string, isSuccess: boolean, onCancelClick: () => void}
) => {
    return (
        <TextField
            size="small"
            sx={{width: width}}
            value={fileName}
            InputProps={{
                sx: {fontSize: 13},
                readOnly: true,
                startAdornment: (
                    <InputAdornment position="start">
                        {isSuccess ? <CheckIcon color="success"/> : <ErrorOutlineIcon color="error"/>}
                    </InputAdornment>
                ),
                endAdornment: (
                    <InputAdornment position="end">
                        <IconButton color="error" size="small" onClick={onCancelClick}>
                            <ClearIcon/>
                        </IconButton>
                    </InputAdornment>
                )
            }}
        />
    );
};

type Props <TField extends FieldValues> = {
    name: Path<TField>,
    control: Control<TField>,
    onDrop: (file: File, fieldName: Path<TField>) => void,
    required?: boolean,
    width?: number | string,
    disabled?: boolean,
};

export const InputFile = <
    TField extends FieldValues
> (
    {
        name,
        control,
        onDrop,
        required = false,
        width = 336,
        disabled = false,
    }: Props<TField>
) => {
    const theme = useTheme();

    const [rejected, setRejected] = useState(false);

    const btnSx = {
        width: "36px",
        maxWidth: "36px",
        minWidth: "36px",
        height: "36px",
        borderBottomLeftRadius: 0,
        borderTopLeftRadius: 0,
        borderBottomRightRadius: 3,
        borderTopRightRadius: 3,
    };
    const dropzoneTypographySx = {ml: 1, fontSize: 13, color: theme.palette.grey["500"]};

    const accept = {
        "image/jpeg": [".jpeg", ".png", ".jpg"],
        "application/pdf": [".pdf"],
    };

    return (
        <Controller
            name={name}
            control={control}
            rules={{
                required: required && "Необходимо заполнить",
            }}
            render={({field: {value, onChange}, fieldState: {error}}) => (
                <Stack>
                    {
                        (!value || rejected) && <Dropzone
                            multiple={false}
                            accept={accept}
                            disabled={disabled}
                            onDrop={(acceptedFiles, fileRejections) => {
                                if (acceptedFiles[0]) {
                                    onChange(acceptedFiles[0]);
                                    onDrop(acceptedFiles[0], name);
                                }

                                fileRejections.length > 0 ? setRejected(true) : setRejected(false);
                            }}
                        >
                            {({getRootProps, getInputProps}) => (
                                <Paper
                                    sx={{
                                        textAlign: "center",
                                        cursor: disabled ? "auto" : "pointer",
                                        borderColor: (error || rejected) ? theme.palette.error.main : theme.palette.grey.A400,
                                        height: 36,
                                        width: width,
                                    }}
                                    variant="outlined"
                                    {...getRootProps()}
                                >
                                    <Stack direction="row" alignItems="center" justifyContent="space-between" >
                                        <Stack>
                                            <input {...getInputProps()} name={name}/>
                                            <Typography sx={dropzoneTypographySx} variant="body1">
                                                Выберите или перетащите файл
                                            </Typography>
                                        </Stack>

                                        <Stack justifyContent="flex-end">
                                            <StyledButton sx={btnSx} variant="contained" disabled={disabled}>
                                                <FileUploadIcon sx={{width: 16, height: 16}}/>
                                            </StyledButton>
                                        </Stack>
                                    </Stack>
                                </Paper>
                            )}
                        </Dropzone>
                    }
                    {value && !rejected && <LoadedFile
                        fileName={(value as File).name}
                        width={width}
                        isSuccess={!error}
                        onCancelClick={() => onChange(null)}
                    />}
                    {error && <FormHelperText sx={{fontSize: 11, mt: 0.6}} error>{error.message}</FormHelperText>}
                    {rejected && <FormHelperText sx={{fontSize: 11}} error>Недопустимый формат файла</FormHelperText>}
                </Stack>
            )}
        />
    );
};