import {GlobalState} from "little-state-machine";
import {LlcInfo} from "../entities/limited-liability-company";
import {SoleProprietorInfo} from "../entities/sole-proprietor";

export function updateInfo(
    state: GlobalState,
    payload: {
        details:  {
            info: SoleProprietorInfo | LlcInfo | null;
            kind?: "llc" | "sp",
        }
    }
): GlobalState {
    return {
        ...state,
        ...payload
    };
}