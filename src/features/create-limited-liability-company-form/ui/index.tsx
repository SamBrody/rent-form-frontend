import {Box, FormControl, Typography} from "@mui/material"
import Grid2 from "@mui/material/Unstable_Grid2";
import {CheckboxField, DatePickerField, Form, FormLabelStyled, Input, InputFile, SubmitButton} from "../../../shared";
import {DefaultValues, Path, SubmitHandler, useForm} from "react-hook-form";
import dayjs, {Dayjs} from "dayjs";
import {Attachment} from "../../../entities/attachments";
import {useUploadFile} from "../../upload-file";
import {useEffect, useState} from "react";
import {useSnackbar} from "notistack";
import {useNavigate} from "@tanstack/react-router";
import {useStateMachine} from "little-state-machine";
import {updateInfo} from "../../updateInfo.ts";
import {LlcInfo} from "../../../entities/limited-liability-company";
import llcData from "../../../shared/llcData.json"

type FormValues = {
    llcFullName: string,
    llcShortName: string,
    registeredAt: Dayjs | null,
    inn: string,
    innScan: File | null,
    innScanGuid: string,
    ogrn: string,
    ogrnScan: File | null,
    ogrnScanGuid: string,
    egripScan: File | null,
    egripScanGuid: string,
    rentContractScan: File | null,
    rentContractScanGuid?: string,
    isNoAgreement: boolean,
};

const defaultValues: DefaultValues<FormValues> = {
    llcFullName: "",
    llcShortName: "",
    inn: "",
    innScan: null,
    innScanGuid: "",
    ogrn: "",
    ogrnScan: null,
    ogrnScanGuid: "",
    registeredAt: null,
    egripScan: null,
    egripScanGuid: "",
    rentContractScanGuid: "",
    isNoAgreement: false,
};

export const CreateLimitedLiabilityCompanyForm = () => {
    const {
        handleSubmit,
        control,
        setError,
        setValue,
        clearErrors,
        watch,
    } = useForm<FormValues>({mode: "onChange", defaultValues});

    const {actions} = useStateMachine({updateInfo});

    const snackbar = useSnackbar();

    const navigate = useNavigate();

    const [fieldName, setFieldName] = useState<Path<FormValues>>();
    const watchIsNoAgreement = watch("isNoAgreement");
    const watchRentContractScan = watch("rentContractScan");
    const watchInn = watch("inn");

    const handleUploadSuccess = (attachment: Attachment) => {
        const msg = `Файл ${attachment.name} успешно загружен`;

        clearErrors(fieldName);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        setValue(`${fieldName}Guid`, attachment.guid);

        snackbar.enqueueSnackbar(msg, {variant: "success"});
    };
    const handleUploadError = () => {
        const msg = "Произошла ошибка при загрузке файла";
        setError(fieldName!, {message: msg});
    };
    const uploadFile = useUploadFile(handleUploadSuccess, handleUploadError);

    useEffect(() => {
        if (watchInn.length !== 10) return;

        const data = llcData.find(x => x.inn === watchInn);

        if (!data) return;

        setValue("llcFullName", data.llcFullName);
        setValue("llcShortName", data.llcShortName);
        setValue("registeredAt", dayjs(new Date(data.registeredAt)));
        setValue("ogrn", data.ogrn);

        clearErrors(["llcFullName", "llcShortName", "registeredAt", "ogrn"]);
    }, [watchInn]);

    const onSubmit: SubmitHandler<FormValues> = (values) => {
        if (!values.isNoAgreement && !values.rentContractScanGuid) {
            const msg = "Добавьте скан договора аренды ИЛИ отметьте \"Нет договора\""
            snackbar.enqueueSnackbar(msg, {variant: "error"});

            return;
        }

        const info: LlcInfo = {
            llcFullName: values.llcFullName,
            llcShortName: values.llcShortName,
            registeredAt: values.registeredAt!.toJSON(),
            inn: values.inn,
            innScanGuid: values.innScanGuid,
            ogrn: values.ogrn,
            ogrnScanGuid: values.ogrnScanGuid,
            egripScanGuid: values.egripScanGuid,
            rentContractScanGuid: values.rentContractScanGuid,
            isNoAgreement: values.isNoAgreement,
        };

        actions.updateInfo({details: {info: info, kind: "llc"}});

        return navigate({to: "/bank-requisites"})
    }

    const onDrop = (file: File, fieldName: Path<FormValues>) => {
        setFieldName(fieldName);

        uploadFile.mutate(file);
    };

    return(
        <Box sx={{flexGrow: 1}}>
            <Typography sx={{mb: 3}} component="h1" variant="h6">Общество с ограниченной ответственностью (ООО)</Typography>

            <Form onSubmit={handleSubmit(onSubmit)}>
                <Grid2 flexDirection="column" container spacing={2}>
                    <Grid2 container>
                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required>Наименование полное</FormLabelStyled>
                                <Input
                                    name="llcFullName"
                                    control={control}
                                    width={512}
                                    placeholder={"ООО \"Московкая промышленная компания\""}
                                    required
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required>Наименование сокрашенное</FormLabelStyled>
                                <Input
                                    name="llcShortName"
                                    control={control}
                                    width={336}
                                    placeholder={"ООО \"МПК\""}
                                    required
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Дата регистрации</FormLabelStyled>
                                <DatePickerField name="registeredAt" control={control} required/>
                            </FormControl>
                        </Grid2>
                    </Grid2>

                    <Grid2 container>
                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required>ИНН</FormLabelStyled>
                                <Input
                                    name="inn"
                                    control={control}
                                    mask="XXXXXXXXXX"
                                    replacement="X"
                                    required
                                    regex="^([0-9]{10})?$"
                                    regexErrorMsg="Необходимо ввести 10 цифр"
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Скан ИНН</FormLabelStyled>
                                <InputFile name="innScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >ОГРН</FormLabelStyled>
                                <Input
                                    name="ogrn"
                                    mask="XXXXXXXXXXXXXXX"
                                    replacement="X"
                                    control={control}
                                    required
                                    regex="^([0-9]{13})?$"
                                    regexErrorMsg="Необходимо ввести 13 цифр"
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Скан ОГРН</FormLabelStyled>
                                <InputFile name="ogrnScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>
                    </Grid2>

                    <Grid2 container>
                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Сканы выписки из ЕГРИП (не старше 3 месяцев)</FormLabelStyled>
                                <InputFile name="egripScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled>Скан договора аренды помещения (офиса)</FormLabelStyled>
                                <InputFile name="rentContractScan" control={control} onDrop={onDrop} disabled={watchIsNoAgreement}/>
                            </FormControl>
                        </Grid2>

                        <Grid2 sx={{mt: {md: 3, lg: 3}}}>
                            <CheckboxField name="isNoAgreement" control={control} disabled={!!watchRentContractScan}/>
                        </Grid2>
                    </Grid2>

                    <Grid2 container justifyContent="flex-end" sx={{mt: 5}}>
                        <SubmitButton>Далее</SubmitButton>
                    </Grid2>
                </Grid2>
            </Form>
        </Box>
    )
}