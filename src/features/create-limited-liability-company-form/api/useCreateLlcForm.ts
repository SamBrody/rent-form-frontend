import {useNavigate} from "@tanstack/react-router";
import {createLlcForm, CreateLlcFormDto} from "../../../entities/limited-liability-company";
import {useMutation} from "@tanstack/react-query";
import {useSnackbar} from "notistack";

export const useCreateLlcForm = () => {
    const snackbar = useSnackbar();

    const navigate = useNavigate();

    return useMutation({
        mutationFn: async(dto: CreateLlcFormDto) => createLlcForm(dto),
        onSuccess: () => {
            snackbar.enqueueSnackbar("Анкета сохранена", {variant: "success"});

            return navigate({to: "/"});
        },
        onError: () => snackbar.enqueueSnackbar("Произошла ошибка при сохранении", {variant: "error"}),
    })
}