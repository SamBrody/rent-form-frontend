import {useMutation} from "@tanstack/react-query";
import {Attachment, postFile} from "../../../entities/attachments";

export const useUploadFile = (onSuccess: (file: Attachment) => void, onError: () => void) => useMutation({
    mutationFn: (file: File) => postFile(file),
    onSuccess: file => onSuccess(file),
    onError: onError,
});