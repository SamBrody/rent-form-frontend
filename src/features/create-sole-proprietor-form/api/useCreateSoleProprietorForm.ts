import {useMutation} from "@tanstack/react-query";
import {useNavigate} from "@tanstack/react-router";
import {createSoleProprietorForm, CreateSoleProprietorFormDto} from "../../../entities/sole-proprietor";
import {useSnackbar} from "notistack";

export const useCreateSoleProprietorForm = () => {
    const snackbar = useSnackbar();

    const navigate = useNavigate();

    return useMutation({
        mutationFn: async(dto: CreateSoleProprietorFormDto) => createSoleProprietorForm(dto),
        onSuccess: () => {
            snackbar.enqueueSnackbar("Анкета сохранена", {variant: "success"});

            return navigate({to: "/"});
        },
        onError: () => snackbar.enqueueSnackbar("Произошла ошибка при сохранении", {variant: "error"}),
    })
}