﻿import Grid2 from "@mui/material/Unstable_Grid2";
import {Box, FormControl, Typography} from "@mui/material";
import {CheckboxField, DatePickerField, Form, FormLabelStyled, Input, InputFile, SubmitButton} from "../../../shared";
import {DefaultValues, Path, SubmitHandler, useForm} from "react-hook-form";
import {Dayjs} from "dayjs";
import {useUploadFile} from "../../upload-file";
import {Attachment} from "../../../entities/attachments";
import {useSnackbar} from "notistack";
import {useState} from "react";
import {useNavigate} from "@tanstack/react-router";
import {useStateMachine} from "little-state-machine";
import {updateInfo} from "../../updateInfo.ts";
import {SoleProprietorInfo} from "../../../entities/sole-proprietor";

type FormValues = {
    inn: string,
    innScan: File | null,
    innScanGuid: string,
    ogrnip: string,
    ogrnipScan: File | null,
    ogrnipScanGuid: string,
    registeredAt: Dayjs | null,
    egripScan: File | null,
    egripScanGuid: string,
    rentContractScan: File | null,
    rentContractScanGuid?: string,
    isNoAgreement: boolean,
};

const defaultValues: DefaultValues<FormValues> = {
    inn: "",
    innScan: null,
    innScanGuid: "",
    ogrnip: "",
    ogrnipScan: null,
    ogrnipScanGuid: "",
    registeredAt: null,
    egripScan: null,
    egripScanGuid: "",
    rentContractScanGuid: "",
    isNoAgreement: false,
};

export const CreateSoleProprietorForm = () => {
    const {
        handleSubmit,
        control,
        setError,
        setValue,
        clearErrors,
        watch,
    } = useForm<FormValues>({mode: "onChange", defaultValues});

    const {actions} = useStateMachine({updateInfo});

    const snackbar = useSnackbar();

    const navigate = useNavigate();

    const [fieldName, setFieldName] = useState<Path<FormValues>>();
    const watchIsNoAgreement = watch("isNoAgreement");
    const watchRentContractScan = watch("rentContractScan");

    const handleUploadSuccess = (attachment: Attachment) => {
        const msg = `Файл ${attachment.name} успешно загружен`;

        clearErrors(fieldName);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        setValue(`${fieldName}Guid`, attachment.guid);

        snackbar.enqueueSnackbar(msg, {variant: "success"});
    };
    const handleUploadError = () => {
        const msg = "Произошла ошибка при загрузке файла";
        setError(fieldName!, {message: msg});
    };
    const uploadFile = useUploadFile(handleUploadSuccess, handleUploadError);

    const onSubmit: SubmitHandler<FormValues> = (values) => {
        if (!values.isNoAgreement && !values.rentContractScanGuid) {
            const msg = "Добавьте скан договора аренды ИЛИ отметьте \"Нет договора\""
            snackbar.enqueueSnackbar(msg, {variant: "error"});

            return;
        }

        const info: SoleProprietorInfo = {
            inn: values.inn,
            innScanGuid: values.innScanGuid,
            ogrnip: values.ogrnip,
            ogrnipScanGuid: values.egripScanGuid,
            registeredAt: values.registeredAt!.toJSON(),
            egripScanGuid: values.egripScanGuid,
            rentContractScanGuid: values.rentContractScanGuid,
            isNoAgreement: values.isNoAgreement,
        };

        actions.updateInfo({details: {info: info, kind: "sp"}});

        return navigate({to: "/bank-requisites"})
    };

    const onDrop = (file: File, fieldName: Path<FormValues>) => {
        setFieldName(fieldName);

        uploadFile.mutate(file);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Typography sx={{mb: 3}} component="h1" variant="h6">Индивидуальный предприниматель (ИП)</Typography>

            <Form onSubmit={handleSubmit(onSubmit)}>
                <Grid2 flexDirection="column" container spacing={2}>
                    <Grid2 container>
                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required>ИНН</FormLabelStyled>
                                <Input
                                    name="inn"
                                    control={control}
                                    mask="XXXXXXXXXX"
                                    replacement="X"
                                    required
                                    regex="^([0-9]{10})?$"
                                    regexErrorMsg="Необходимо ввести 10 цифр"
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Скан ИНН</FormLabelStyled>
                                <InputFile name="innScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >ОГРНИП</FormLabelStyled>
                                <Input
                                    name="ogrnip"
                                    mask="XXXXXXXXXXXXXXX"
                                    replacement="X"
                                    control={control}
                                    required
                                    regex="^([0-9]{15})?$"
                                    regexErrorMsg="Необходимо ввести 15 цифр"
                                />
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Скан ОГРНИП</FormLabelStyled>
                                <InputFile name="ogrnipScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>
                    </Grid2>

                    <Grid2 container>
                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Дата регистрации</FormLabelStyled>
                                <DatePickerField name="registeredAt" control={control} required/>
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled required >Сканы выписки из ЕГРИП (не старше 3 месяцев)</FormLabelStyled>
                                <InputFile name="egripScan" control={control} onDrop={onDrop} required/>
                            </FormControl>
                        </Grid2>

                        <Grid2>
                            <FormControl>
                                <FormLabelStyled>Скан договора аренды помещения (офиса)</FormLabelStyled>
                                <InputFile name="rentContractScan" control={control} onDrop={onDrop} disabled={watchIsNoAgreement}/>
                            </FormControl>
                        </Grid2>

                        <Grid2 sx={{mt: {md: 3, lg: 3}}}>
                            <CheckboxField name="isNoAgreement" control={control} disabled={!!watchRentContractScan}/>
                        </Grid2>
                    </Grid2>

                    <Grid2 container justifyContent="flex-end" sx={{mt: 5}}>
                        <SubmitButton>Далее</SubmitButton>
                    </Grid2>
                </Grid2>
            </Form>
        </Box>
    )
}