﻿import {useQuery} from "@tanstack/react-query";
import {getBankRequisitesByBik} from "../../../entities/bank-requisites";

export const useGetBankRequisitesByBik = (bik: string) => useQuery({
        queryKey: ["bankRequisites", bik],
        queryFn: () => getBankRequisitesByBik(bik),
    }
)