﻿import {FieldValues, SubmitHandler, useFieldArray, useForm} from "react-hook-form";
import {Box, Divider, FormControl, IconButton, Stack, Typography} from "@mui/material";
import {Form, FormLabelStyled, Input, StyledButton, SubmitButton} from "../../../shared";
import Grid2 from "@mui/material/Unstable_Grid2";
import {InfoTooltip} from "./info-tooltip.tsx";
import {CompleteButton} from "./complete-button.tsx";
import {useGetBankRequisitesByBik} from "../api/useGetBankRequisitesByBik.ts";
import {useEffect, useState} from "react";
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import {useStateMachine} from "little-state-machine";
import {updateInfo} from "../../updateInfo.ts";
import {BankRequisites} from "../../../entities/bank-requisites";
import {CreateSoleProprietorFormDto, SoleProprietorInfo} from "../../../entities/sole-proprietor";
import {useCreateSoleProprietorForm} from "../../create-sole-proprietor-form";
import {CreateLlcFormDto, LlcInfo} from "../../../entities/limited-liability-company";
import {useCreateLlcForm} from "../../create-limited-liability-company-form";

type BankRequisitesValues = {
    bik: string,
    branchName: string,
    bankAccount: string,
    correspondentAccount: string,
};

type FormValues = FieldValues & {
    requisites: Array<BankRequisitesValues>,
};

export const CreateBankRequisites = () => {
    const {state} = useStateMachine({updateInfo});

    const {
        control,
        setValue,
        setError,
        clearErrors,
        handleSubmit,
        watch,
    } = useForm<FormValues>(
        {
            mode: "onChange",
            defaultValues: {requisites: [
                {
                    bik: "",
                    branchName: "",
                    bankAccount: "",
                    correspondentAccount: "",
                }
            ]}
        }
    );

    const {
        fields,
        append,
        remove
    } = useFieldArray<FormValues>({control, name: "requisites"});

    const createSpForm = useCreateSoleProprietorForm();
    const createLlcForm = useCreateLlcForm();

    const [fieldIndex, setFieldIndex] = useState(0);

    const watchBik = watch(`requisites.${fieldIndex}.bik`);

    const {data, isSuccess, isRefetching} = useGetBankRequisitesByBik(watchBik);

    const checkBik = () => {
        if (watchBik?.length !== 9 || !watchBik) {
            setError(`requisites.${fieldIndex}.bik`, {type: "required", message: "Необходимо заполнить"});

            return false;
        }

        if (data?.error) {
            // Сообщение с API
            setError(`requisites.${fieldIndex}.bik`, {type: "required", message: data.error});

            return false;
        }

        clearErrors(`requisites.${fieldIndex}.bik`);
        return true;
    };

    const setBranchName = () => {
        const branchName = isSuccess ? data.name : "";
        setValue(`requisites.${fieldIndex}.branchName`, branchName);
        clearErrors(`requisites.${fieldIndex}.branchName`);
    };

    const setCorrespondentAccount = () => {
        const correspondentAccount = isSuccess ? data.ks : "";
        setValue(`requisites.${fieldIndex}.correspondentAccount`, correspondentAccount);
        clearErrors(`requisites.${fieldIndex}.correspondentAccount`);
    };

    const onSetBranchNameClick = () => {
        const isValid = checkBik();

        if (isValid) setBranchName();
    }

    const onSetCorrespondentAccountClick = () => {
        const isValid = checkBik();

        if (isValid) setCorrespondentAccount();
    }

    useEffect(() => {
        if (!checkBik()) return;

        setBranchName();
        setCorrespondentAccount();
    }, [isSuccess, isRefetching]);

    const onSubmit: SubmitHandler<FormValues> = (values) => {
        const requisites = values.requisites.map(x => {
            const r: BankRequisites = {
                bik: x.bik,
                bankAccount: x.bankAccount,
                branchName: x.branchName,
                correspondentAccount: x.correspondentAccount,
            }

            return r;
        });

        if (state.details.kind === "sp") handleSpForm(requisites);
        if (state.details.kind === "llc") handleLlcForm(requisites);
    };

    const handleLlcForm = (bankRequisites: BankRequisites[]) => {
        const info = state.details.info as LlcInfo;

        const dto: CreateLlcFormDto = {
            llcFullName: info.llcFullName,
            llcShortName: info.llcShortName,
            registeredAt: info.registeredAt,
            inn: info.inn,
            innScanGuid: info.innScanGuid,
            ogrn: info.ogrn,
            ogrnScanGuid: info.ogrnScanGuid,
            egripScanGuid: info.egripScanGuid,
            rentContractScanGuid: info.rentContractScanGuid,
            isNoAgreement: info.isNoAgreement,
            bankRequisites: bankRequisites,
        }

        createLlcForm.mutate(dto);
    };

    const handleSpForm = (bankRequisites: BankRequisites[]) => {
        const info = state.details.info as SoleProprietorInfo;

        const dto: CreateSoleProprietorFormDto = {
            inn: info.inn,
            innScanGuid: info.innScanGuid,
            ogrnip: info.ogrnip,
            ogrnipScanGuid: info.ogrnipScanGuid,
            registeredAt: info.registeredAt,
            egripScanGuid: info.egripScanGuid,
            rentContractScanGuid: info.rentContractScanGuid,
            isNoAgreement: info.isNoAgreement,
            bankRequisites: bankRequisites,
        }

        createSpForm.mutate(dto);
    };

    return(
        <Box sx={{ flexGrow: 1 }}>
            <Typography sx={{mb: 3}} component="h1" variant="h6">Банковские реквизиты</Typography>

            <Form onSubmit={handleSubmit(onSubmit)}>
                {
                    fields.map((item, index) => {
                        return (
                            <Grid2 key={item.id} flexDirection="column" container spacing={2}>
                                <Stack flexDirection="row" spacing={1}>
                                    <Grid2 container spacing={1}>
                                        <Grid2 container>
                                            <Grid2 sx={{maxWidth: 200}}>
                                                <FormControl>
                                                    <FormLabelStyled required>БИК</FormLabelStyled>
                                                    <Input
                                                        name={`requisites.${index}.bik`}
                                                        control={control}
                                                        onInput={() => setFieldIndex(index)}
                                                        mask="XXXXXXXXX"
                                                        replacement="X"
                                                        required
                                                        regex="^([0-9]{9})?$"
                                                        regexErrorMsg="Необходимо ввести 9 цифр"
                                                    />
                                                </FormControl>
                                            </Grid2>

                                            <Grid2>
                                                <FormControl>
                                                    <FormLabelStyled required>Название филиала банка</FormLabelStyled>
                                                    <Stack direction="row" alignItems="center" spacing={1}>
                                                        <Input
                                                            name={`requisites.${index}.branchName`}
                                                            control={control}
                                                            width={512}
                                                            placeholder={"ООО \"Московкая промышленная компания\""}
                                                            required
                                                            endAdornment={<CompleteButton onClick={() => {
                                                                setFieldIndex(index);
                                                                onSetBranchNameClick();
                                                            }}/>}
                                                        />
                                                        <InfoTooltip title="Автоматическое заполнение названия филиала банка по БИК"/>
                                                    </Stack>
                                                </FormControl>
                                            </Grid2>
                                        </Grid2>

                                        <Grid2 container>
                                            <Grid2>
                                                <FormControl>
                                                    <FormLabelStyled required>Рассчетный счет</FormLabelStyled>
                                                    <Input
                                                        name={`requisites.${index}.bankAccount`}
                                                        control={control}
                                                        width={248}
                                                        mask="XXXXXXXXXXXXXXXXXXXX"
                                                        replacement="X"
                                                        required
                                                        regex="^([0-9]{20})?$"
                                                        regexErrorMsg="Необходимо ввести 20 цифр"
                                                    />
                                                </FormControl>
                                            </Grid2>

                                            <Grid2>
                                                <FormControl>
                                                    <FormLabelStyled required>Корреспондентский счет</FormLabelStyled>
                                                    <Stack direction="row" alignItems="center" spacing={1}>
                                                        <Input
                                                            name={`requisites.${index}.correspondentAccount`}
                                                            control={control}
                                                            width={336}
                                                            mask="XXXXXXXXXXXXXXXXXXXX"
                                                            replacement="X"
                                                            required
                                                            regex="^([0-9]{20})?$"
                                                            regexErrorMsg="Необходимо ввести 20 цифр"
                                                            endAdornment={<CompleteButton onClick={() => {
                                                                setFieldIndex(index);
                                                                onSetCorrespondentAccountClick();
                                                            }}/>}
                                                        />
                                                        <InfoTooltip title="Автоматическое заполнение корреспондентского счета по БИК"/>
                                                    </Stack>
                                                </FormControl>
                                            </Grid2>
                                        </Grid2>
                                    </Grid2>

                                    <Grid2>
                                        <IconButton
                                            disabled={fields.length === 1}
                                            size="small"
                                            color="error"
                                            onClick={() => remove(index)}
                                        >
                                            <DeleteIcon/>
                                        </IconButton>
                                    </Grid2>
                                </Stack>

                                <Divider sx={{my: 2}}/>
                            </Grid2>
                        )
                    })
                }

                <Stack direction="row" justifyContent="space-between" sx={{mt: 1}}>
                    <Stack justifyContent="flex-start">
                        <StyledButton
                            onClick={() => append({
                                bik: "", branchName: "", bankAccount: "", correspondentAccount: ""
                            })}
                            startIcon={<AddIcon/>}
                        >
                            Добавить ещё один банк
                        </StyledButton>

                    </Stack>

                    <Stack justifyContent="flex-end">
                        <SubmitButton>Отправить</SubmitButton>
                    </Stack>
                </Stack>
            </Form>
        </Box>
    )
}