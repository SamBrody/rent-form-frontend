﻿import {StyledButton} from "../../../shared";

type Props = {
    onClick?: () => void;
}

export const CompleteButton = ({onClick}: Props) => {
    return (
        <StyledButton onClick={onClick}>Заполнить</StyledButton>
    )
}