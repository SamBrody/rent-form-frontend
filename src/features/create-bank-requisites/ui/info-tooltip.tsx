﻿import InfoIcon from '@mui/icons-material/Info';
import {styled, Tooltip, tooltipClasses, TooltipProps} from "@mui/material";

const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} arrow classes={{ popper: className }} />
))(() => ({
    [`& .${tooltipClasses.arrow}`]: {
        "&::before": {
            backgroundColor: "rgb(246, 251, 254)",
            border: "solid",
            borderWidth: "1px",
            borderColor: "rgb(182,224, 246)",
        }
    },
    [`& .${tooltipClasses.tooltip}`]: {
        backgroundColor: "rgb(246, 251, 254)",
        border: "solid",
        borderWidth: "1px",
        borderColor: "rgb(182,224, 246)",
        color: "rgb(94, 94, 88)",
        fontSize: 11,
        textAlign: "center",
        padding: 10,
        width: 180,
    },
}));


type Props = {
    title: string,
};

export const InfoTooltip = ({title}: Props) => {
    const size = 16;

    return(
        <LightTooltip title={title} arrow placement="top">
            <InfoIcon sx={{width: size, height: size}} color="info"/>
        </LightTooltip>
    );
};