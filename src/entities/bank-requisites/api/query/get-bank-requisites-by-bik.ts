﻿import {bikInfoAxiosClient} from "../../../../shared/api/base.ts";
import {BankRequisitesDto} from "../dto/bank-requisites.dto.ts";

export const getBankRequisitesByBik = async(bik: string): Promise<BankRequisitesDto> => {
    if (!bik || bik.length !== 9) return Promise.reject();

    const response = await bikInfoAxiosClient.get<BankRequisitesDto>(`/api.html?type=json&bik=${bik}`);

    return response.data;
}