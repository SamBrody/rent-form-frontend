﻿export type BankRequisitesDto = {
    bik: string,
    ks: string,
    name: string,
    error: string,
};