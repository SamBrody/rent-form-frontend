﻿export type BankRequisites = {
    bik: string,
    branchName: string,
    bankAccount: string,
    correspondentAccount: string,
}