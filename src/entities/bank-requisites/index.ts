﻿export type {BankRequisites} from "./model/bank-requisites.ts"
export type {BankRequisitesDto} from "./api/dto/bank-requisites.dto.ts"
export {getBankRequisitesByBik} from "./api/query/get-bank-requisites-by-bik.ts"