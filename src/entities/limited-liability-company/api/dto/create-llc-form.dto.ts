import {BankRequisites} from "../../../bank-requisites";

export type CreateLlcFormDto = {
    llcFullName: string,
    llcShortName: string,
    registeredAt: string,
    inn: string,
    innScanGuid: string,
    ogrn: string,
    ogrnScanGuid: string,
    egripScanGuid: string,
    rentContractScanGuid?: string,
    isNoAgreement: boolean,
    bankRequisites: BankRequisites[],
}