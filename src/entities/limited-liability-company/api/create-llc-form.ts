import {axiosClient} from "../../../shared";
import {CreateLlcFormDto} from "./dto/create-llc-form.dto.ts";

export const createLlcForm = async(dto: CreateLlcFormDto) => {
    return await axiosClient.post("/limited-liability-companies/forms", dto);
}