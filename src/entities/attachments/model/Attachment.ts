export type Attachment = {
    guid: string,
    name: string,
};