import {Attachment} from "../model/Attachment.ts";
import {axiosClient} from "../../../shared";

export const postFile = async (file: File) : Promise<Attachment> => {
    const formData = new FormData();

    formData.append("attachment", file);
    const response = await axiosClient.post<FormData, Attachment>(
        "attachments/", formData, { headers: {'Content-Type' : 'multipart/form-data', 'Accept': 'text/plain'}}
    );

    return response.data;
};