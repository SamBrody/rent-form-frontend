﻿export type SoleProprietorInfo = {
    inn: string,
    innScanGuid: string,
    ogrnip: string,
    ogrnipScanGuid: string,
    registeredAt: string,
    egripScanGuid: string,
    rentContractScanGuid?: string,
    isNoAgreement: boolean,
}