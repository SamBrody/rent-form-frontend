import {BankRequisites} from "../../../bank-requisites";

export type CreateSoleProprietorFormDto = {
    inn: string,
    innScanGuid: string,
    ogrnip: string,
    ogrnipScanGuid: string,
    registeredAt: string,
    egripScanGuid: string,
    rentContractScanGuid?: string,
    isNoAgreement: boolean,
    bankRequisites: BankRequisites[],
}