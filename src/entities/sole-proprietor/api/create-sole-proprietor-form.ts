import {axiosClient} from "../../../shared";
import {CreateSoleProprietorFormDto} from "./dto/create-sole-proprietor-form.dto.ts";

export const createSoleProprietorForm = async(dto: CreateSoleProprietorFormDto) => {
    return await axiosClient.post("/sole-proprietors/forms", dto);
}