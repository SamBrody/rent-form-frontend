﻿import {CreateLimitedLiabilityCompanyForm} from "../../../features/create-limited-liability-company-form";

export const LimitedLiabilityCompanyPage = () => (<CreateLimitedLiabilityCompanyForm/>);