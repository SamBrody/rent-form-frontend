﻿import {Autocomplete, FormControl, FormLabel, Stack, TextField, Typography} from "@mui/material";
import {Outlet, useNavigate} from "@tanstack/react-router";
import {MainContainer} from "./main-container.tsx";
import {useEffect, useState} from "react";

const formKinds = [
    { id: "SP", label: "Индивидуальный предприниматель (ИП)", route: "/sole-proprietors" },
    { id: "LLC", label: "Общество с ограниченной ответственностью (ООО)", route: "/limited-liability-companies" },
]

export const MainPage = () => {
    const navigate = useNavigate();

    const [formKind, setFormKind] = useState<{id: string, label: string, route: string} | null>(null);

    useEffect(() => {
        navigate({to: formKind ? formKind.route : "/"});
    }, [formKind]);

    return(
        <MainContainer>
            <Stack spacing={5}>
                <Typography component="h1" variant="h6">Форма собственности</Typography>

                <FormControl>
                    <FormLabel id="ac-kind" required sx={{mb: 1}}>Вид деятельности</FormLabel>
                    <Autocomplete
                        id="ac-kind"
                        value={formKind}
                        onChange={(_, value) => setFormKind(value)}
                        size="small"
                        sx={{width: '512px'}}
                        getOptionLabel={(option) => option.label}
                        isOptionEqualToValue={(option, value) =>
                            value === undefined || option.id === value.id
                        }
                        renderInput={(params) => <TextField {...params}/>}
                        options={formKinds}
                    />
                </FormControl>

                <Outlet/>
            </Stack>
        </MainContainer>
    )
}