﻿import {Container} from "@mui/material";
import {ReactNode} from "react";

type Props = {
    children: ReactNode;
}

export const MainContainer = ({children, ...props} : Props) => {
    const sx = {
        display: "flex",
        flexDirection: "column",
        marginTop: 10,
        spacing: 5,
    };

    return(
        <Container
            sx={sx}
            component="main"
            maxWidth="lg"
            {...props}
        >
            {children}
        </Container>
    );
};