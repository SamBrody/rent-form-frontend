﻿import {RootRoute, Route, Router} from "@tanstack/react-router";
import {MainPage} from "../pages/main";
import {LimitedLiabilityCompanyPage} from "../pages/limited-liability-company";
import {SoleProprietorPage} from "../pages/sole-proprietor";
import {BankRequisitesPage} from "../pages/bank-requisites";

export const routeRoot = new RootRoute();

const mainRoute = new Route({
    getParentRoute: () => routeRoot,
    path: "/",
    component: MainPage,
});

const llcRoute = new Route({
    getParentRoute: () => mainRoute,
    path: "/limited-liability-companies",
    component: LimitedLiabilityCompanyPage,
});

const spRoute = new Route({
    getParentRoute: () => mainRoute,
    path: "/sole-proprietors",
    component: SoleProprietorPage,
});

const bankRequisitesRoute = new Route({
    getParentRoute: () => mainRoute,
    path: "/bank-requisites",
    component: BankRequisitesPage,
})

const routeTree = routeRoot.addChildren([mainRoute]);

mainRoute.addChildren([llcRoute, spRoute, bankRequisitesRoute]);

export const router = new Router({routeTree: routeTree});