import {SoleProprietorInfo} from "../entities/sole-proprietor";
import {LlcInfo} from "../entities/limited-liability-company";

declare module "little-state-machine" {
    interface GlobalState {
        details:  {
            info: SoleProprietorInfo | LlcInfo | null;
            kind?: "llc" | "sp",
        }
    }
}