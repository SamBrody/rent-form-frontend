import React from 'react'
import ReactDOM from 'react-dom/client'
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";
import {RouterProvider} from "@tanstack/react-router";
import {router} from "./router.ts";
import dayjs from "dayjs";
import ru from "dayjs/locale/ru";
import updateLocale from "dayjs/plugin/updateLocale";
import {SnackbarProvider} from "notistack";
import {createStore, StateMachineProvider} from "little-state-machine";

const client = new QueryClient();

dayjs.locale(ru)
dayjs.extend(updateLocale);
dayjs.updateLocale('ru', {
    months: [
        'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
    ],
});

createStore({
    details: {
        info: null,
        kind: undefined,
    }
})

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <QueryClientProvider client={client}>
          <StateMachineProvider>
              <SnackbarProvider>
                  <RouterProvider router={router}/>
              </SnackbarProvider>
          </StateMachineProvider>
      </QueryClientProvider>
  </React.StrictMode>
)
