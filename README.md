# Архитектура

За основу была взята методология FSD (Feature-Sliced Design), предназначенная для frontend-приложений. Она помогает сделать проект понятным и структурированным.

Для автозаполнения информации о банках используется API ```https://bik-info.ru```.

Для автозаполнения информации о ООО - заготовленный файл ```llcData.json```.

# Стек

React + TypeScript + Vite + mui, react-hook-forms, tanstack/react-query, tanstack/react-router, axios

# Запуск

По пути ``` src/shared/api/config ``` лежит файл index.ts с константой **RENT_API_URL**, значение этой констаны - адрес **rent-form-backend**.
По умолчанию это значение равно ``` http://127.0.0.1:3105/v1 ```, то есть адрес инстанса, запущенного в docker-container. При необходимости этот адрес нужно поменять на свой.

1. Перейти в каталог:  ```C:\Users\szh\WebstormProjects\rent-form-frontend>> ```

2. Выполнить: ```npm install``` для установки требуемых пакетов

3. Выполнить: ```npm run dev``` для запуска приложения

4. Перейти (по умолчанию) по адресу: ```http://localhost:5173/```